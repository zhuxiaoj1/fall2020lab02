// Danilo Zhu 1943382 Assignment 2

package lab02.eclipse;

import java.util.Random;

/**
 * @author Danilo Zhu 1943382
 */
public class BikeStore {
    public static void main(String[] args) {
        Random ra = new Random();
        Bicycle[] stock = new Bicycle[4];

        stock[0] = new Bicycle("Amateur", 18, 21.36);
        stock[1] = new Bicycle("Veteran", 21, 26.92);
        stock[2] = new Bicycle("Specialized", 24, 23.36);
        stock[3] = new Bicycle("Amateur", 27, 20.51);

        for(Bicycle i : stock) {
            System.out.println(i.toString());
        }
    }
}
